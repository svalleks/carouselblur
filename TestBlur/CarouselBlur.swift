//
//  CarouselBlur.swift
//  TestBlur
//
//  Created by Sergey Aleksandrov on 15/03/2018.
//  Copyright © 2018 Sergey Aleksandrov. All rights reserved.
//

import Foundation
import iCarousel
import FXBlurView


class CarouselWithBlur: iCarousel {
    
    var blurView: FXBlurView!
    
    open func applyBlur(radius: CGFloat, underlyingView: UIView) {
        blurView = createBlur(radius: radius, underlyingView: underlyingView)
        blurView.layer.mask = createMaskForItems()        
    }
    
    open func updateBlur() {
        guard let cellSpacing = delegate?.carousel?(self, valueFor: .spacing, withDefault: 1), let mask = blurView?.layer.mask else {
            return
        }
        let offset = offsetForItem(at: currentItemIndex)
        let cellLeading = (frame.width - itemWidth) / 2
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        mask.frame.origin.x = cellLeading + itemWidth * cellSpacing * (offset - 1)
        CATransaction.commit()
    }
    
    fileprivate func createBlur(radius: CGFloat, underlyingView: UIView) -> FXBlurView {
        let blurView = FXBlurView(frame: frame)
        blurView.blurRadius = radius
        blurView.isDynamic = false
        blurView.underlyingView = underlyingView
        superview?.insertSubview(blurView, belowSubview: self)
        return blurView
    }

    fileprivate func createMaskForItems() -> CALayer {
        let maskLayer = CALayer()
        if let maskImage = createMaskImage() {
            let mask = CGImage(maskWidth: maskImage.cgImage!.width, height: maskImage.cgImage!.height, bitsPerComponent: maskImage.cgImage!.bitsPerComponent, bitsPerPixel: maskImage.cgImage!.bitsPerPixel, bytesPerRow: maskImage.cgImage!.bytesPerRow, provider: maskImage.cgImage!.dataProvider!, decode: nil, shouldInterpolate: true)
            
            maskLayer.contents = mask
            maskLayer.frame.size = CGSize(width: maskImage.size.width / UIScreen.main.scale, height: maskImage.size.height / UIScreen.main.scale)
            maskLayer.frame.origin = CGPoint(x: (frame.width - maskLayer.frame.width) / 2, y: 0)
        }
        return maskLayer
    }
    
    fileprivate func createMaskImage() -> UIImage? {
        guard let cellSpacing = delegate?.carousel?(self, valueFor: .spacing, withDefault: 1) else {
            return nil
        }
        let cellMaskSize = CGSize(width: itemWidth * UIScreen.main.scale, height: frame.height * UIScreen.main.scale)
        let cellWithSpacingWidth = cellMaskSize.width * cellSpacing
        let numberOfVisibleCells = Int(frame.size.width / itemWidth) + 2
        let maskSize = CGSize(width: cellWithSpacingWidth * CGFloat(numberOfVisibleCells), height: cellMaskSize.height)
        
        UIGraphicsBeginImageContext(maskSize)
        let context = UIGraphicsGetCurrentContext()!
        context.setFillColor(UIColor.white.cgColor)
        context.fill(CGRect(origin: .zero, size: maskSize))
        context.setFillColor(UIColor.black.cgColor)
        
        for i in 0 ..< numberOfVisibleCells {
            let itemOrigin = CGPoint(x: CGFloat(i) * cellWithSpacingWidth, y: 0)
            let itemFrame = CGRect(origin: itemOrigin, size: cellMaskSize)
            let cornerRadius = currentItemView?.layer.cornerRadius ?? 0
            if cornerRadius > 0 {
                let path = UIBezierPath(roundedRect: itemFrame, cornerRadius: cornerRadius)
                path.fill()
            } else {
                context.fill(itemFrame)
            }
        }
        let maskImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return maskImage
    }
}
