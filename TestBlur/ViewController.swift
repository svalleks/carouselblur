//
//  ViewController.swift
//  TestBlur
//
//  Created by Admin on 21.02.17.
//  Copyright © 2017 Sergey Aleksandrov. All rights reserved.
//

import UIKit
import iCarousel
import FXBlurView
import PureLayout

class ViewController: UIViewController, iCarouselDelegate, iCarouselDataSource {
    
    @IBOutlet weak var carouselView: CarouselWithBlur!
    @IBOutlet weak var imageView: UIImageView!
    
    var cellSize: CGSize {
        return CGSize(width: carouselView.frame.width * 0.6, height: carouselView.frame.height)
    }
    var cellSpacing: CGFloat = 1.1
    var cornerRadius: CGFloat = 30
    
    // MARK: - ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.layoutIfNeeded()
        carouselView.type = .linear
        carouselView.applyBlur(radius: 20, underlyingView: imageView)        
    }
    
    // MARK: - iCarouselDataSource
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 10
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var transparentView: TransparentView
        if view == nil {
            transparentView = .fromNib()
            transparentView.frame.size.width = cellSize.width
            transparentView.layer.cornerRadius = cornerRadius
        } else {
            transparentView = view as! TransparentView
        }
        return transparentView
    }

    // MARK: - iCarouselDelegate

    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch option {
        case .wrap:
            return 1.0
        case .spacing:
            return cellSpacing
        default:
            return value
        }
    }
    
    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
        return cellSize.width
    }
    
    func carouselDidScroll(_ carousel: iCarousel) {
        (carousel as? CarouselWithBlur)?.updateBlur()
    }
}

extension UIView {
    class func fromNib<T : UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
